﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sc_CamManager : MonoBehaviour
{

    public GameObject player;
    private Rigidbody rb;
    public Animator an;

    void Start()
    {
        rb = player.GetComponent<Rigidbody>();
    }

    void Update()
    {
        an.SetFloat("velocity", rb.velocity.magnitude);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sc_PlayerController : MonoBehaviour
{

    public GameObject player;
    public GameObject head;
    public Rigidbody rb;
    public float speed;
    public float maxspeed;
    public float sensetivity;
    
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }
    
    void Update()
    {
        rb.AddForce(transform.forward * speed * Input.GetAxis("Vertical"));

        rb.AddForce(transform.right * speed * Input.GetAxis("Horizontal"));

        if (rb.velocity.magnitude > maxspeed)
        {
            rb.velocity /= rb.velocity.magnitude / maxspeed;
        }

        transform.Rotate(new Vector3(0, sensetivity*Input.GetAxis("Mouse X"), 0));
        head.transform.Rotate(new Vector3(-sensetivity * Input.GetAxis("Mouse Y"), 0, 0));
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sc_GoalScore : MonoBehaviour
{
    public string team;

    void Start()
    {

    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag(team))
        {
            if (string.Equals(team, "red"))
            {
                GameObject.FindWithTag("gm").GetComponent<Sc_GameManager>().redscore++;
            }

            if (string.Equals(team, "blue"))
            {
                GameObject.FindWithTag("gm").GetComponent<Sc_GameManager>().bluescore++;
            }
        }
    }
}
